% calculate transport across a transect across the mouth of Lochaline using
% WLLS model output.

% run Quick_PLot_WLLS_currents to load data and plot domain figures etc.

WLLScoast = read_sms_map([getenv('SSM') '/Coastlines/WLLS.map']);

% define transect using GB National grid coordinates obtaned from GIS
transect_xy = [167965 744528; 168137 744528];

[transect_ll(:,2), transect_ll(:,1)] = os2ll(transect_xy(:,1), transect_xy(:,2));

% save FVCOM grid in GB national grid
[M.x, M.y] = ll2os(M.lat, M.lon);
[M.xc, M.yc] = ll2os(M.latc, M.lonc);

% add additional grid variables
M.hc = mean(M.h(M.nv),2);

%% make figure showing transect and location previously chosen in channel mouth

plot_fvcom_field(M, M.h, 'pll', 'axi', axis_lims, 'cli', [0 150])
hold on
% plot(region_boundary2(:,1), region_boundary2(:,2), '-r')
plot(transect_ll(:,1), transect_ll(:,2), '-r', 'linewidth', 2)
plot(M.lonc(Ie1(3)), M.latc(Ie1(3)), 'xr')

c = colorbar;
set(get(c, 'xlabel'), 'string', 'mean depth (m)');
box on

set(gcf,'paperpositionmode','auto','paperunits','centimeters','paperposition',[0. 0. 20 20])
print('-dpng', '-r300', ['Lochaline_transect.png'])

%% setup intput for transport calculation

% find elements on the line within 200 m and sort them in order of easting
dist = 200;
data.Ie1 = find(M.xc>transect_xy(1,1) & M.xc<transect_xy(2,1) & M.yc>transect_xy(1,2)-dist & M.yc<transect_xy(2,2)+dist);
tmp = double(M.xc(data.Ie1));
[~,Isort]=sort(tmp);
data.Ie = data.Ie1(Isort);

data.h = M.hc(data.Ie);
data.x = M.xc(data.Ie);
data.y = M.yc(data.Ie);

In = M.nv(data.Ie,:);

%% make figure showing elements used to calculate transect

% axis_lims_xy([1 3]) = ll2os(axis_lims(3), axis_lims(1));
% axis_lims_xy([2 4]) = ll2os(axis_lims(4), axis_lims(2));
%plot_fvcom_field(M, M.h, 'axi', axis_lims_xy, 'cli', [0 150])
plot_fvcom_field(M, M.h)
hold on
% plot(region_boundary2(:,1), region_boundary2(:,2), '-r')
% plot(transect_ll(:,1), transect_ll(:,2), '-r', 'linewidth', 2)
plot(data.x, data.y, '.-r')

axis(1E-5*[1.6634    1.7030    7.4092    7.4766])
c = colorbar;
set(get(c, 'xlabel'), 'string', 'mean depth (m)');
box on


%%
%% transport calculation
data.ua = ua(Ie,:);
data.va = va(Ie,:);
data.h  = M.hc(Ie);
data.x = M.xc(Ie,:);
data.y = M.yc(Ie,:);

% rough and ready - just use one of the adjacent nodes - probably change
% this at some point.
data.zeta = zeta(In(:,1),:);

data.time = time;

Trans = fun_transport_NS_2d(data);

%% plot transport and velocity time series
time2 = time-time(1);
figure
subplot(211)
plot(time2, Trans.Q, time2([1 end]), mean(Trans.Q)*[1 1])
ylabel('Transport (m^3 s^{-1})')
grid on

subplot(212)
plot(time2, ua(Ie1(3),:), time2, va(Ie1(3),:))
grid on

xlabel('Time (days since 1 April)')
ylabel('Velocity (m s^{-1})')
legend('U velocity', 'V velocity')

set(gcf,'paperpositionmode','auto','paperunits','centimeters','paperposition',[0. 0. 20 20])
print('-dpng', '-r300', ['Lochaline_transport.png'])

%% Work out volume of Lochaline using WLLS model batheymetry

% loch in bounded by this area
lochaline.lims = [axis_lims2([1 2]) transect_ll(1,2) axis_lims2(4)];
plot_fvcom_field(M, M.h, 'pll', 'axi', lochaline.lims, 'cli', [0 40])

% find nodes and elements within Lochaline
lochaline.In = find(M.lon>=lochaline.lims(1) & M.lon<=lochaline.lims(2) & M.lat>=lochaline.lims(3) & M.lat<=lochaline.lims(4));
lochaline.Ie = find(M.lonc>=lochaline.lims(1) & M.lonc<=lochaline.lims(2) & M.latc>=lochaline.lims(3) & M.latc<=lochaline.lims(4));
% hold on, plot(M.lonc(lochaline.Ie), M.latc(lochaline.Ie), 'xr')

% work out area of each element within Lochaline
lochaline.Ae = fun_element_area(M, lochaline.Ie);

% volume of each element
lochaline.Ve = M.hc(lochaline.Ie)'.*lochaline.Ae;

% total volume
lochaline.volume = sum(lochaline.Ve);

% time needed to flush 60% of loch
lochaline.FT_secs = 0.6*lochaline.volume ./ mean(Trans.Q);
lochaline.FT_days = lochaline.FT_secs ./ (60*60*24);

return
%%
figure
hold on
fun_plot_sms_coast(WLLScoast, 'k', 'plot', 1:length(WLLScoast.node), 1);
plot(M.lonc(lochaline.Ie), M.latc(lochaline.Ie), 'xr')
