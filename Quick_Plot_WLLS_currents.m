clear, close all

% BaseName = ['/home/rory/Downloads/WLLS_Climatology2_']; 
BaseName = [getenv('SSM') '/WLLS/WLLS_1.02_Climatology/output/WLLS_Climatology2_'];

M = read_fvcom_grid_data([BaseName '0099.nc']);
M.lon(M.lon>180) = M.lon(M.lon>180)-360;
M.lonc(M.lonc>180) = M.lonc(M.lonc>180)-360;
M.lonE = M.lon(M.nv)'; M.latE = M.lat(M.nv)';

axis_lims = [-5.85 -5.7 56.47 56.565];
axis_lims2 = [-5.775 -5.74 56.5375 56.565];
region_boundary = [axis_lims([1 2 2 1 1])', axis_lims([3 3 4 4 3])'];
region_boundary2 = [axis_lims2([1 2 2 1 1])', axis_lims2([3 3 4 4 3])'];

pos = [-5.78 56.52; -5.76 56.55; -5.7724 56.5372];
Ie1 = fun_nearest2D(pos(:,1), pos(:,2), M.lonc, M.latc);
In1 = fun_nearest2D(pos(:,1), pos(:,2), M.lon, M.lat);

Ie = find(M.lonc>axis_lims(1) & M.lonc<axis_lims(2) & M.latc>axis_lims(3) & M.latc<axis_lims(4));
In = find(M.lon>axis_lims(1) & M.lon<axis_lims(2) & M.lat>axis_lims(3) & M.lat<axis_lims(4));
Ie2 = find(M.lonc>axis_lims2(1) & M.lonc<axis_lims2(2) & M.latc>axis_lims2(3) & M.latc<axis_lims2(4));
In2 = find(M.lon>axis_lims2(1) & M.lon<axis_lims2(2) & M.lat>axis_lims2(3) & M.lat<axis_lims2(4));


% SSMcoast = read_sms_map([getenv('SSM') '/Coastlines/WLLS.map']);

%%
figure
plot_fvcom_field(M, M.h, 'pll', 'fid', subplot(221))
hold on
plot(region_boundary(:,1), region_boundary(:,2), '-r')
c = colorbar;
set(get(c, 'xlabel'), 'string', 'mean depth (m)');
box on

plot_fvcom_field(M, M.h, 'pll', 'axi', axis_lims, 'cli', [0 150], 'fid', subplot(222))
hold on
plot(pos(:,1), pos(:,2), 'wx');
plot(region_boundary2(:,1), region_boundary2(:,2), '-r')
c = colorbar;
set(get(c, 'xlabel'), 'string', 'mean depth (m)');
box on

plot_fvcom_field(M, M.h, 'pll', 'axi', axis_lims2, 'cli', [-10 40], 'grd', 'k', 'fid', subplot(223))
hold on
plot(pos(:,1), pos(:,2), 'wx');
colormap(jet(10));
c = colorbar;
set(get(c, 'xlabel'), 'string', 'mean depth (m)');
box on

set(gcf,'paperpositionmode','auto','paperunits','centimeters','paperposition',[0. 0. 20 20])
print('-dpng', '-r300', ['WLLS_domain.png'])

%% load in elevation and currents data from 15 files
time = []; ua = []; va = []; zeta = [];
% for ii=99:100
for ii=92:106 
    time = [time; ncread([BaseName num2str(ii, '%04d') '.nc'], 'time')];
    ua = [ua, ncread([BaseName num2str(ii, '%04d') '.nc'], 'ua')];
    va = [va, ncread([BaseName num2str(ii, '%04d') '.nc'], 'va')];
%     u = ncread([BaseName num2str(ii, '%04d') '.nc'], 'u');
%     v = ncread([BaseName num2str(ii, '%04d') '.nc'], 'v');
%     ua = [ua, squeeze(mean(u,2))];
%     va = [va, squeeze(mean(v,2))];
    zeta = [zeta, ncread([BaseName num2str(ii, '%04d') '.nc'], 'zeta')];
end

%ua = squeeze(mean(u,2));
%va = squeeze(mean(v,2));
sa = sqrt( ua.^2 + va.^2 );

%%
% It1 = 20;
It1 = 188;
It = [0:12]+It1;
rangeI = 1:2:length(Ie);
quiverData.U = ua(Ie(rangeI),It);
quiverData.V = va(Ie(rangeI),It);
quiverData.X = M.lonc(Ie(rangeI));
quiverData.Y = M.latc(Ie(rangeI));


quiverData2.U = ua(Ie2,It);
quiverData2.V = va(Ie2,It);
quiverData2.X = M.lonc(Ie2);
quiverData2.Y = M.latc(Ie2);
%% plot velocity time series at locations identified
t0 = time(It1);

time_hrs = (time-t0)*24;

figure
subplot(311)
plot(time_hrs, zeta(In1,:))
hold on
plot((time(It1)-t0)*[1 1], [-2 2])
ylabel('Water elevation (m)')
grid on

subplot(312)
plot(time_hrs, ua(Ie1,:))
hold on
plot((time(It1)-t0)*[1 1], [-0.5 0.5])
ylabel('U vel (m/s)')
grid on

subplot(313)
plot(time_hrs, va(Ie1,:))
hold on
plot((time(It1)-t0)*[1 1], [-2 2])
ylabel('V vel (m/s)')
xlabel('Time (Hourse since HW)')
grid on

set(gcf,'paperpositionmode','auto','paperunits','centimeters','paperposition',[0. 0. 20 18])
print('-dpng', '-r300', ['Lochaline_timeseries.png'])

return
%% Animation for one day
% plot_fvcom_field(M, sa(:,It), 'pll', 'axi', axis_lims, 'qui', quiverData, 'gif', 'WLLS_animation.gif', 'cli', [0 1])


%% manual animation
close all

figure
quiverData_tmp1.X = quiverData.X;
quiverData_tmp1.Y = quiverData.Y;
quiverData_tmp2.X = quiverData2.X;
quiverData_tmp2.Y = quiverData2.Y;
for tt=1:length(It)
    quiverData_tmp1.U = quiverData.U(:,tt);
    quiverData_tmp1.V = quiverData.V(:,tt);
    quiverData_tmp2.U = quiverData2.U(:,tt);
    quiverData_tmp2.V = quiverData2.V(:,tt);
    subplot(121)
    hold off
    patch(M.lonE, M.latE, sa(:,It(tt))', 'linestyle', 'none')
    hold on
    quiver(quiverData_tmp1.X, quiverData_tmp1.Y, quiverData_tmp1.U, quiverData_tmp1.V, 'color', 0.99*[1 1 1]);
    axis(axis_lims)
    caxis([0 1.5])
    %plot_fvcom_field(M, sa(:,It(tt)), 'pll', 'axi', axis_lims, 'qui', quiverData_tmp1, 'cli', [0 2], 'fid', subplot(121), 'tit', ['Spring HW + ' num2str(tt-1) 'H']);
    axis off
    colorbar off
    c1 = colorbar('location', 'SouthOutside');
    set(get(c1, 'xlabel'), 'string', 'Speed (m/s)')
    title(['Spring HW + ' num2str(tt-1) 'H'])
    
    subplot(122)
    hold off
    patch(M.lonE, M.latE, sa(:,It(tt))', 'linestyle', 'none')
    hold on
    quiver(quiverData_tmp2.X, quiverData_tmp2.Y, quiverData_tmp2.U, quiverData_tmp2.V, 'color', 0.99*[1 1 1]);
    axis(axis_lims2)
    caxis([0 0.5])
    %plot_fvcom_field(M, sa(:,It(tt)), 'pll', 'axi', axis_lims2, 'qui', quiverData_tmp2, 'cli', [0 0.5], 'fid', subplot(122), 'tit', ['Spring HW + ' num2str(tt-1) 'H']);
    
    axis off
    colorbar off
    c2 = colorbar('location', 'SouthOutside');
    set(get(c2, 'xlabel'), 'string', 'Speed (m/s)')
    title(['Spring HW + ' num2str(tt-1) 'H'])
    colormap(jet(10))
    
    set(gcf,'paperpositionmode','auto','paperunits','centimeters','paperposition',[0. 0. 20 15])
    print('-dpng', '-r300', ['WLLS_' num2str(tt, '%02d') '.png'])
    %pause
end



