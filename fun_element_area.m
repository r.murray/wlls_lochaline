function A = fun_element_area(M, Ie)
for ii=1:length(Ie)
    nodes = M.nv(Ie(ii),:);
    x = M.x(nodes); y = M.y(nodes);
    dx1 = x(1) - x(2);
    dy1 = y(1) - y(2);
    
    dd1 = sqrt(dx1.^2 + dy1.^2);
    x(4) = 0.5*(x(1)+x(2));
    y(4) = 0.5*(y(1)+y(2));
    
    dx2 = x(3)-x(4); dy2 = y(3)-y(4);
    dd2 = sqrt(dx2.^2 + dy2.^2);
    
    A(ii) = 0.5.*dd1.*dd2;
end